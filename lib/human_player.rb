class HumanPlayer
  attr_reader :name
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def get_move
    puts "Where would you like to move? row, column."
    gets.chomp.split(",").map(&:to_i)
  end

  def display(board)
    board.grid.each do |row|
      str = ""
      row.each do |sym|
        if sym == :X
          str += "X"
        elsif sym == :O
          str += "O"
        else
          str += "-"
        end
      end
      puts str
    end
  end

end
