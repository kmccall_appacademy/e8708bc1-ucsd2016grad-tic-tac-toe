class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    self.all_moves.each do |move|
      if self.winner?(move)
        return move
      end
    end
    self.all_moves.sample
  end

  def winner?(move)
    # make a duplicate board
    @board.place_mark(move, mark)
    if (@board.winner != mark)
      @board.place_mark(move, nil)
      false
    else
      true
    end
  end

  def all_moves
    moves = []
    i = 0
    while i < 3
      j = 0
      while j < 3
        if @board.empty?([i, j])
          moves << [i, j]
        end
        j += 1
      end
      i += 1
    end
    moves
  end

end
