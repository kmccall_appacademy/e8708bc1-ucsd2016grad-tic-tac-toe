class Board

  attr_reader :grid

  def initialize(grid = Array.new(3) {Array.new(3)})
    @grid = grid
  end

  def place_mark(pos, mark)
    @grid[pos[0]][pos[1]] = mark
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def board_full?
    # i = 0
    # while i < 3
    #   j = 0
    #   while j < 3
    #     if self.empty?(i, j)
    #       return false
    #     end
    #   end
    # end
    # true

    @grid.all? do |row|
      row.all? {|i| i != nil}
    end
  end

  def winner
    col = self.columns
    diags = self.diagonals

    rows = (@grid + col + diags)
    rows.each do |row|
      if row.all? {|item| item == :X }
        return :X
      elsif row.all? {|item| item == :O }
        return :O
      end
    end
    return nil
  end

  def columns
    col = []
    i = 0
    while i < 3
      sub = []
      @grid.each do |row|
        sub << row[i]
      end
      col << sub
      i += 1
    end
    col
  end

  def diagonals
    diag1 = []
    i = 0
    while i < 3
      diag1 << @grid[i][i]
      i += 1
    end
    diag2 = [@grid[0][2], @grid[1][1], @grid[2][0]]
    [diag1, diag2]
  end

  def over?
    if !self.winner.nil?
      true
    elsif self.board_full?
      true
    else
      false
    end
  end
end
