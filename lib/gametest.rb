require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require_relative 'game'

human = HumanPlayer.new("susan")
human.mark = :X
comp = ComputerPlayer.new("miladbot")
comp.mark = :O
game = Game.new(comp, human)

while !game.board.over?
  game.play_turn
end
puts "the winner is #{game.board.winner}"
